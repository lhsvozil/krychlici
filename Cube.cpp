#ifndef _CUBECPP
#define _CUBECPP

#include "Cube.h"
#include <GL/glu.h>
#include <GL/glut.h>
#include "Weapon.h"
#include "Enviroment.h"
#include <string>
#include <sstream>
#include "AnGeo.h"
#include <stdexcept>
//template <typename T> std::string tostr(const T& t) { std::ostringstream os; os<<t; return os.str();


CordXY Cube::getPosition(){
return m_position;
}

void Cube::setPosition(CordXY position){
m_position=position;
}
CordXYZ Cube::getRealPosition(){
return m_realPosition;
}
CordXYZ Cube::EnvPosToRealPos(CordXY posInEnv){
   CordXYZ result;
   result.x= posInEnv.x*m_size+0.5*m_size;
   result.z= posInEnv.y*m_size+0.5*m_size;
   result.z*=-1;
   result.y=0;
return result;
}

void Cube::takeHit(Projectile* from){
    m_hp-=from->getDmg();

    if (m_hp<0){
        die();
    }else{
        if (m_animationAction.blinking==false){
            m_animationAction.blinking=true;
            m_blinkCounter=0;
            MyEnv->addToAnimeList(this);
        }else{
            m_blinkCounter-=8;
        }
    }

    //MyEnv->logThis(tostr(m_hp));
}
void Cube::takeHit(Cube* from){
    m_hp-=from->m_strength;
    if (m_hp<0){
        die();
    }else{
        if (m_animationAction.blinking==false){
            m_animationAction.blinking=true;
            m_blinkCounter=0;
            MyEnv->addToAnimeList(this);
        }else{
            m_blinkCounter-=8;
        }
    }
}
void Cube::die(){

    canMove=false;
    MyEnv->removeFromAnimeList(this);
    MyEnv->removeCube(this);
    MyEnv->m_explosions.push_back(new Explosion(getRealPosition(),m_size,70,m_originalColor,MyEnv));
    //MyEnv->logThis("diied");
    delete this;
}
void Cube::draw(CordXY position) {
    m_position=position;
    glPushMatrix();

    glTranslatef(m_realPosition.x,m_realPosition.y,m_realPosition.z);
    glRotatef(m_angle,0,1,0);
	/*glBegin(GL_QUADS);

	//Top face
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glNormal3f(0.0, 1.0f, 0.0f);
	glVertex3f(-m_size / 2, m_size / 2, -m_size / 2);
	glVertex3f(-m_size / 2, m_size / 2, m_size / 2);
	glVertex3f(m_size / 2, m_size / 2, m_size / 2);
	glVertex3f(m_size / 2, m_size / 2, -m_size / 2);

	//Bottom face
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glNormal3f(0.0, -1.0f, 0.0f);
	glVertex3f(-m_size / 2, -m_size / 2, -m_size / 2);
	glVertex3f(m_size / 2, -m_size / 2, -m_size / 2);
	glVertex3f(m_size / 2, -m_size / 2, m_size / 2);
	glVertex3f(-m_size / 2, -m_size / 2, m_size / 2);

	//Left face
	glNormal3f(-1.0, 0.0f, 0.0f);
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(-m_size / 2, -m_size / 2, -m_size / 2);
	glVertex3f(-m_size / 2, -m_size / 2, m_size / 2);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-m_size / 2, m_size / 2, m_size / 2);
	glVertex3f(-m_size / 2, m_size / 2, -m_size / 2);

	//Right face
	glNormal3f(1.0, 0.0f, 0.0f);
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(m_size / 2, -m_size / 2, -m_size / 2);
	glVertex3f(m_size / 2, m_size / 2, -m_size / 2);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(m_size / 2, m_size / 2, m_size / 2);
	glVertex3f(m_size / 2, -m_size / 2, m_size / 2);


	//Front face
	glNormal3f(0.0, 0.0f, 1.0f);
glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(-m_size / 2, -m_size / 2, m_size / 2);

	glVertex3f(m_size / 2, -m_size / 2, m_size / 2);

	glVertex3f(m_size / 2, m_size / 2, m_size / 2);

	glVertex3f(-m_size / 2, m_size / 2, m_size / 2);

	//Back face
	glNormal3f(0.0, 0.0f, -1.0f);
glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(-m_size / 2, -m_size / 2, -m_size / 2);

	glVertex3f(-m_size / 2, m_size / 2, -m_size / 2);

	glVertex3f(m_size / 2, m_size / 2, -m_size / 2);

	glVertex3f(m_size / 2, -m_size / 2, -m_size / 2);

	glEnd();*/
    glColor3f(m_color.red,m_color.green, m_color.blue);
	glutSolidCube(m_size);
	//glScalef(5,5,5);
	//glutSolidTetrahedron();
	glPopMatrix();
}

int Cube::getID(){
return m_ID;
}
int Cube::getTeam(){
    return m_team;
}
Cube::Cube(float size, Color color, Enviroment* env, CordXY posInEnv,int team){
    m_hp=50;
    MyEnv=env;
    m_ID = MyEnv->GrabID();
    m_team=team;
    m_size=size;
	m_color=color;
	m_directionVector={0,0,-10};
	m_weapon=new Weapon(env,1.4,{0.9,0.9,0.9},20,0.6,m_ID);
	m_strength=30;
	m_angle=0;
    m_angleChange=15;
    m_directionAngle=0;
    m_blinkColor={0.3,0.3,0.3};
    m_originalColor=m_color;

    m_position=posInEnv;
    m_lastPosInEnv=m_position;
    m_realPosition.x= m_position.x*m_size+0.5*m_size;
    m_realPosition.z= m_position.y*m_size+0.5*m_size;
    m_realPosition.z*=-1;
    m_realPosition.y=0;

    MyEnv->addObjToPos(posInEnv,this);
    MyEnv->addCubeToTeam(this,m_team);
    canMove=true;

}
void Cube::animate(){
    if (m_animationAction.attacking){

        m_angle+=m_angleChange;
        if (m_angle<180){
            m_realPosition.y+=0.08;
        }else {
            m_realPosition.y-=0.08;
        }
        if (m_angle>=360){
            MyEnv->removeFromAnimeList(this);
            m_angle=0;
            m_realPosition.y=0;
            canMove=true;
            m_animationAction.attacking=false;
        }
    }else if (m_animationAction.moving){
        if (AnGeo::distanceOfPoints(m_realPosition,EnvPosToRealPos(m_position))>AnGeo::sizeOfVec(m_moveVec)){
            m_realPosition=m_realPosition+m_moveVec;
        }else{
            MyEnv->removeFromAnimeList(this);
            m_realPosition=EnvPosToRealPos(m_position);
            m_animationAction.moving=false;
            canMove=true;
        }
    }else if (m_animationAction.rotating){
        if (abs(m_directionAngle-m_angle)>m_angleChange/3){
            m_angle+=(m_angle-m_directionAngle>0) ? -m_angleChange/3 : m_angleChange/3;  //


        }else{
            m_angle=m_directionAngle;
            MyEnv->removeFromAnimeList(this);
            m_animationAction.rotating=false;
            canMove=true;
        }
    }else if (m_animationAction.blinking){
        if (m_blinkCounter < 8){
            m_blinkCounter++;
            if (m_blinkCounter%2==1){
                m_blinkColor=m_color+m_blinkColor;
                m_color=m_blinkColor-m_color;
                m_blinkColor=m_blinkColor-m_color;
            }

        }else{

            MyEnv->removeFromAnimeList(this);
            m_animationAction.blinking=false;
            m_color=m_originalColor;
        }
    }

}

void Cube::changeMyDirection(CordXY newDirVec){
    canMove=false;
    m_animationAction.rotating=true;
    MyEnv->addToAnimeList(this);
    m_directionAngle=AnGeo::vectorToAngleDeg(newDirVec);
    m_directionVector={newDirVec.x,0,-newDirVec.y};
    //MyEnv->logThis(m_directionAngle);
}

bool Cube::move(CordXY direction){

    if (canMove){
        if (MyEnv->moveCube(this,direction)){
           canMove=false;
           m_moveVec={AnGeo::normalizeVec(m_position-m_lastPosInEnv).x,0,-(AnGeo::normalizeVec(m_position-m_lastPosInEnv).y)}; // << - ?
           m_moveVec=m_moveVec*0.65;
           //MyEnv->logThis(m_moveVec);
           m_lastPosInEnv=m_position;

           m_animationAction.moving=true;
           MyEnv->addToAnimeList(this);
           return true;
        }
    }
    return false;
}

void Cube::attack(){
    if (canMove){
        canMove=false;
        MyEnv->addToAnimeList(this);
        m_animationAction.attacking=true;

    CordXY smery[]={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};
        for (int i=0;i<8;i++){
            GraphObj* obj = MyEnv->getObjAtPos(m_position+smery[i]);
            if (obj!=nullptr){
                obj->takeHit(this);
            }
        }
    }
}

void Cube::Fire(){

    Projectile* bullet=new Projectile(m_weapon->m_projectile);
    bullet->setDirection(AnGeo::normalizeVec(m_directionVector));

    bullet->setPosition(getRealPosition());
    MyEnv->addActiveProjectile(bullet);
    bullet=nullptr;
}
void Cube::fireAt(Cube* cube){
    if (cube!=nullptr){
        m_directionVector=(cube->getRealPosition()-getRealPosition());
        Projectile* bullet=new Projectile(m_weapon->m_projectile);
        bullet->setDirection(AnGeo::normalizeVec(m_directionVector));

        bullet->setPosition(getRealPosition());
        MyEnv->addActiveProjectile(bullet);
        bullet=nullptr;
    }
}

#endif // _CUBECPP
