#ifndef ANIMATIONACTIONSTRUCT_H_INCLUDED
#define ANIMATIONACTIONSTRUCT_H_INCLUDED

enum Actions {NONE,ATTACKING,MOVING,ROTATING,BLINKING};
struct AnimationActionStruct /// struktura obsahuje ruzne stavy kostek pro �cely animaci
{
    bool attacking;
    bool moving;
    bool rotating;
    bool blinking;
    AnimationActionStruct(){
    attacking=false;
    moving=false;
    rotating=false;
    blinking=false;
    }
};

#endif // ANIMATIONACTIONSTRUCT_H_INCLUDED
