#ifndef _MYCUBECPP
#define _MYCUBECPP



#include "MyCube.h"
#include "Weapon.h"
#include <math.h>
#include <cmath>
#include <string>
#include <sstream>
#include "AnGeo.h"
template <typename T> std::string tostr(const T& t) { std::ostringstream os; os<<t; return os.str(); }
MyCube::MyCube(Enviroment* env, CordXY positionInEnv):Cube::Cube(3,{0,0,1},env,positionInEnv,0) {
    m_cameraDistance={1.5*m_size,3};
    m_cameraDistance.x+=3;
    m_cameraDistance.y+=3;
    m_cameraLookAtDistance=3.5*m_size;

    m_cameraLookAtY=10;
    m_fi=90;
    m_hp=300;
    m_strength=80;
    m_energy.replenishRate=4;
    m_energy.shootingMAX=240;
    dead=false;
}

CordXYZ MyCube::getCameraPos(){
    CordXYZ result;
    result.x = m_realPosition.x - m_cameraDistance.x*cosf(m_fi); //m_cameraDistance.x - x znaci horizontalni vzdalenost
    result.z = m_realPosition.z - m_cameraDistance.x*sinf(m_fi); //m_cameraDistance.x - x znaci horizontalni vzdalenost
    //result.z*=-1;

    result.y = m_cameraDistance.y;                                                             //m_cameraDistance.y - y znaci jak uz to byva vertikalni vzdalenost
    return result;
}

CordXYZ MyCube::getCameraLookAtPos(){
    CordXYZ result;
    result.x = m_realPosition.x + m_cameraLookAtDistance*cosf(m_fi);
    result.z = m_realPosition.z + m_cameraLookAtDistance*sinf(m_fi);
   // result.z*=-1;
    result.y = m_cameraLookAtY;
    return result;
}

void MyCube::setFi(float fi){
    m_fi-=fi;

    //MyEnv->logThis(tostr(m_fi));
}
void MyCube::setCameraLookAtY(float y){
    if (m_cameraLookAtY>15 && y>0){}
    else if (m_cameraLookAtY<-15 && y<0){}
    else {
        m_cameraLookAtY+=y;
    }
}

float MyCube::getFiAsAngle(){
    m_directionVector=getCameraLookAtPos()-getCameraPos();
    return (atan2(m_directionVector.x,m_directionVector.z)+3.14159265358979f);
    //MyEnv->logThis(tostr(m_fi));
}

void MyCube::changeCameraDist(int decreaseOrIncrease){
    if (decreaseOrIncrease>0){
        m_cameraDistance.x+=1;
        m_cameraDistance.y+=1;
    }else{
        m_cameraDistance.x-=1;
        m_cameraDistance.y-=1;
    }
}


void MyCube::Fire(){
    if (!dead){ // budouci (m_energy.shooting-=m_weapon->m_energyConsumption)>0
        m_directionVector=getCameraLookAtPos()-getCameraPos();
        m_directionVector.y=0;
        //m_directionVector.z*=-1;
        Projectile* bullet=new Projectile(m_weapon->m_projectile);              /// kk
        bullet->setDirection(AnGeo::normalizeVec(m_directionVector));

        bullet->setPosition(getRealPosition());
        MyEnv->addActiveProjectile(bullet);
        //bullet=nullptr;
        //MyEnv->logThis(getFiAsAngle());
    }
}
void MyCube::die(){
    m_energy.replenishRate=0;
    m_energy.shooting=0;
    dead=true;
    canMove=false;
    MyEnv->removeFromAnimeList(this);
    MyEnv->removeCube(this);
    MyEnv->m_explosions.push_back(new Explosion(getRealPosition(),m_size,30,m_originalColor,MyEnv));
    //MyEnv->logThis("diied");

}



#endif // _MYCUBECPP
