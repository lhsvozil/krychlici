#ifndef ENERGY_H_INCLUDED
#define ENERGY_H_INCLUDED
struct Energy  /// struktura udrzujci stav energii aby krychlici furt nestrileli, v budoucnu nechodily
{
    int shooting;
    int moving;
    int shootingMAX;
    int replenishRate;
    Energy(){
        shooting=0;
        moving=0;
        shootingMAX=120;
        replenishRate=1;
    }
    Energy& operator++(){
        if (shooting<shootingMAX){
                shooting+=replenishRate;
        }
        moving++;
        return *this;
    }
    Energy operator++(int){
        Energy result(*this);
        operator++();
        return result;
  }

};


#endif // ENERGY_H_INCLUDED
