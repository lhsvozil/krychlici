#ifndef _CUBEH
#define _CUBEH

//#include "Weapon.h"
#include "CordXY.h"
#include "CordXYZ.h"
#include "Color.h"
#include "GraphObj.h"
#include "AnimationActionStruct.h"
#include "Energy.h"

class Enviroment;
class Weapon;
class Projectile;

//enum Actions {NONE,ATTACKING,MOVING,ROTATING,BLINKING};
class Cube : public GraphObj    /// hlavni trida ktera definuje krychlika
{
protected:

	Color m_color;
	int m_hp;
	int m_strength;
	int m_team;
	float m_angle;
	float m_angleChange;
	float m_directionAngle;
	//float m_height;
	Weapon* m_weapon;  /// kazdy krychlik muze a nemusi mit zbran, popripade jednoduse muze vzit z mapy jinou zbran
	//CordXY attribute;

    CordXY m_lastPosInEnv;

    CordXYZ m_moveVec;
    Enviroment* MyEnv;
    AnimationActionStruct m_animationAction;
    int m_blinkCounter;
    Color m_blinkColor;
    Color m_originalColor;

public:
    Energy m_energy; /// kazdy krychlik ma energii pro strileni, v budoucnu i pro chozeni atd..
    CordXYZ m_directionVector;
    bool canMove;
    void attack();
    void animate();  /// metoda obstaravajici ruzne animace na zaklade stavu struktuy AnimationActionStruc m_animationAction

    bool move(CordXY direction); /// hybani

	void draw(CordXY position); /// metoda pro vykresleni
    CordXY getPosition();
    CordXYZ getRealPosition();
    CordXYZ EnvPosToRealPos(CordXY PosInEnv);
    void setPosition(CordXY position);
	//Cube(float size, Color color);
	Cube(float size, Color color, Enviroment* env, CordXY posInEnv,int team);
	virtual ~Cube(){ };
   	virtual void takeHit(Projectile* from);  /// metody odecitajici hp
   	void takeHit(Cube* from); /// metody odecitajici hp
    virtual void die(); /// metoda zajistujici odstraneni krychlika z mapy a pameti
	//void takeHit(Cube* from);
    void Fire(); /// strileni
    void fireAt(Cube* cube);
    int getID(); /// vraci unikatni ID objektu v prostredi
    int getTeam(); /// vraci tym, ve kterem krychlik je, krychlici ve stejnem tymu po sobe nestrili, ale mohou se zasahnout
    virtual void update(){};
    virtual void checkPossibleTargets(){}; ///hleda vhodny cil
    void changeMyDirection(CordXY newDirVec); /// nataceni se
};

#endif
