#ifndef EXPLOBJ_H
#define EXPLOBJ_H

#include "CordXYZ.h"
#include "Color.h"
class ExplObj
{
    public:
        ExplObj(CordXYZ position, CordXYZ direction);
        virtual ~ExplObj(){ }
        void draw();     // vyzkouset bez virtual, kvuli perforamnce
        void fly();
        void fade();
    protected:
        CordXYZ m_position;
        CordXYZ m_directionVec;

        CordXYZ m_size;
        Color m_color;
        float m_transparency;
    private:
};

#endif // EXPLOBJ_H
