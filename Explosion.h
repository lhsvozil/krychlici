#ifndef EXPLOSION_H
#define EXPLOSION_H

#include "CordXYZ.h"
#include <vector>
#include "TetraH.h"
#include <random>

class Enviroment;
class Explosion /// trida zajistujici exploze
{
    public:
        Explosion(CordXYZ position, float size, int howBig,Color color,Enviroment* MyEnv);
        ~Explosion();
        void draw();
        void update();
        void fadeAway();
        int getID();
    protected:
        Enviroment* MyEnv;
        int m_ID;
        std::vector<TetraH*> m_objects;
        CordXYZ m_position;
        float m_size;
        int m_howBig;
        std::minstd_rand randGen;
        int m_timeToLive;
    private:
};

#endif // EXPLOSION_H
