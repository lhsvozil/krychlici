#ifndef cordxyzh_
#define cordxyzh_
#include <math.h>

struct CordXYZ  /// 3d bod
{

	float x;
	float y;
	float z;
	CordXYZ operator+ (const CordXYZ& other){
        CordXYZ result={x+other.x,y+other.y,z+other.z};
        return result;
	}
	CordXYZ operator- (const CordXYZ& other){
        CordXYZ result={x-other.x,y-other.y,z-other.z};
        return result;
	}
	CordXYZ operator* (const float number){
        CordXYZ result={x*number,y*number,z*number};
        return result;
	}
	CordXYZ operator/ (const float number){
        CordXYZ result={x/number,y/number,z/number};
        return result;
	}



};




#endif // cordxyzh_
