#ifndef _ENVIROMH
#define _ENVIROMH

#include "GraphObj.h"
#include <vector>
#include "MyCube.h"

#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include <fstream>
#include "Explosion.h"

class Projectile;
class Enviroment   /// v podstate engine cele hry, jednoduchym vytvo�en�m dal�� instance tohoto prost�ed� lze nahr�t jinou mapu s �pln� jinymi krychl�ky
{
private:
	std::vector<std::vector<GraphObj*> > m_env;  /// mapa
    std::vector<Projectile*> m_projectiles; /// aktivni - letici projektily
    //std::vector<Explosion*> m_explosions;
    int m_nextID;
    //int getObjAtPos_succesCount;
    std::ofstream log;
    std::vector<Cube*> m_animList;  /// seznam zrovna se animujicich krychliku
    float m_sizeOfGrid;
    bool lichyCyklus;  /// �prava vykreslovani aby mohl b�t vy��� framerate p�i rozhl�en� se
public:
    std::vector<std::vector<Cube*> > m_cubesByTeam; /// seznam krychl�ku podle t�m�
    std::vector<Explosion*> m_explosions;  /// seznam aktivnich explozi
    void drawFloor();

    MyCube* m_MyCube;
    int m_size;
	void drawScene();

	Enviroment(int size); /// d� se r�ct "defaultn�" konstruktor, pouzije hardcoded testovaci mapu
	Enviroment(std::ifstream& levelMap);  /// na�te mapu ze souboru, zat�m pouze 1.txt, v budoucnu asi z parametru p��kazev� r�dky od nejak�ho launcheru
	void inicVectorsAndVariables();  /// spolecn� c�st obou konstruktor�
	~Enviroment();

	bool moveMyCube(int wasd);
	bool moveCube(Cube* cube,CordXY direction);

	CordXY whereAmI(GraphObj* whoAmI);
    CordXY whereAmI(CordXYZ pos);


    void addObjToPos(CordXY posInEnv,GraphObj* obj);
	GraphObj* getObjAtPos(CordXY pos);
    void removeObj(GraphObj* obj);
    void removeCube(Cube* obj);
    void addToAnimeList(Cube* c);
    void removeFromAnimeList(Cube* c);
    void removeExplosion(Explosion* explo);
	void timerTick();
	void addActiveProjectile(Projectile* bullet);
	int GrabID();
    void addCubeToTeam(Cube* cube, int team);

	void logThis(std::string what);
	void logThis(CordXYZ what);
	void logThis(CordXY what);
	void logThis(float what);
    void logThis(double what);

};
#endif
