#include "Explosion.h"
#include "TetraH.h"
#include "AnGeo.h"
#include "Enviroment.h"

Explosion::Explosion(CordXYZ position, float size, int howBig,Color color,Enviroment* env)
{
    CordXYZ objPos=position;
    CordXYZ objDirVec;
    MyEnv=env;
   m_position=position;
   m_position.y-=1.5;                   //posunuti stredu exploze na podlahu
   m_size=size;
   m_howBig=howBig;
   m_timeToLive=200;
   m_ID=MyEnv->GrabID();


   for (int i=0;i<15*howBig;i++){
        objPos=position+AnGeo::randomRangePoint(-0.5*size,0.5*size);        //nahodna pozice strepu v zanikajici krychli
        objDirVec=(objPos-m_position)*0.40; //z 0.55, mensi 45
        //objDirVec.y=AnGeo::randomRangef(0.4,1.5);
        //if (objDirVec.y<0.1){
        //    objDirVec.y=AnGeo::randomRangef(0.1,0.5);                  //pridani sily strepum miricim do zeme, aby se mohli odrazit
        //}
       // MyEnv->logThis(objDirVec);
        m_objects.push_back(new TetraH(objPos,objDirVec,AnGeo::randomRangePoint(0,5),AnGeo::randomRangef(-50.5,50.5),AnGeo::randomRangePoint(0.005,0.6),color));
   }



}
Explosion::~Explosion(){
    for (auto obj : m_objects){
        delete obj;
    }
}
void Explosion::draw(){
    for (auto obj : m_objects){
        obj->draw();
    }
}

void Explosion::update(){
    if (--m_timeToLive > 0){
        for (auto obj : m_objects){
            obj->fly();
        }
    }else{
        if (m_timeToLive > -80){
            for (auto obj : m_objects){
                obj->fade();
            }
        }else{
             MyEnv->removeExplosion(this);
        }
    }

}
void Explosion::fadeAway(){

}
int Explosion::getID(){
return m_ID;
}
