#ifndef ANGEO_H
#define ANGEO_H
#include "CordXYZ.h"
#include "CordXY.h"

class AnGeo /// trida obsahuje stripky z analyticke geometrie a ruzne pomocne funcke ktere mohou byt definovany staticky
{
    public:
    static float distanceOfPoints(CordXYZ a,CordXYZ b);
    static float distanceOfPoints(CordXY a,CordXY b);
    static float getViewAngle(float r, float sideA);
    static CordXYZ randomRangePoint(float minimum, float maximum);
    static float randomRangef(float minimum, float maximum);
    static CordXYZ normalizeVec(CordXYZ vec);
    static CordXY normalizeVec(CordXY vec);
    static float sizeOfVec(CordXYZ vec);
    static float vectorToAngleDeg(CordXY vec);
    //CordXYZ randomRangePointUniform(float minimum, float maximum);
    //CordXYZ randomRangefUniform(float minimum, float maximum);
    protected:

    private:
};

#endif // ANGEO_H
