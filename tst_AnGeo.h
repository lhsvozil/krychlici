#ifndef TST_ANGEO_H_INCLUDED
#define TST_ANGEO_H_INCLUDED

#include "TestSuite/Suite.h"
#include "TestSuite/Test.h"
#include "AnGeo.h"

class TestAnGeo:public TestSuite::Test{

    void testuj(){
        CordXY a={154,0};
        CordXY b={1,1};
        CordXY c={1,3};
        CordXY d={1,0};
        test_(AnGeo::vectorToAngleDeg({1,0})==0);
        test_(AnGeo::vectorToAngleDeg({0,1})==90);
        test_(AnGeo::distanceOfPoints(b,c)==2);
        test_(AnGeo::normalizeVec(a).x==d.x);
    }

    void run(){
        testuj();
    }
};

#endif // TST_ANGEO_H_INCLUDED
