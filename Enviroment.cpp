#ifndef _ENVIROMCPP
#define _ENVIROMCPP

#include "Enviroment.h"
#include "MyCube.h"
#include "Cube.h"
#include "Wall.h"
#include <stdexcept>
#include "Projectile.h"
#include <algorithm>
#include "AnGeo.h"
#include "EnemyCube.h"
#include <time.h>
#include <limits>
#include "tst_AnGeo.h"
#include "TestSuite/Suite.h"



void Enviroment::drawScene() {


    drawFloor();
	for (int i=0;i<m_size;i++){
        for (int j=0;j<m_size;j++){
            if (m_env.at(i).at(j)!=nullptr){
                m_env.at(i).at(j)->draw({i,j});
            }
        }
	}
	for (auto bullet : m_projectiles){
        if (bullet!=nullptr){
            bullet->draw();
        }
	}
	for (auto explo : m_explosions){
        if (explo!=nullptr){
            explo->draw();
        }
	}


}
void Enviroment::drawFloor(){
     glPushMatrix();
    glTranslatef(m_MyCube->m_size*m_size/2.0,-m_MyCube->m_size/2.0,-m_MyCube->m_size*m_size/2.0);

	glBegin(GL_QUADS);

	//Top face
	glColor3f(0.0,0.4, 0.0);
	glNormal3f(0.0, 1.0f, 0.0f);
	glVertex3f(-m_MyCube->m_size*m_size / 2, 0, -m_MyCube->m_size*m_size / 2);
	glVertex3f(-m_MyCube->m_size*m_size / 2, 0, m_MyCube->m_size*m_size / 2);
	glVertex3f(m_MyCube->m_size*m_size / 2, 0, m_MyCube->m_size*m_size / 2);
	glVertex3f(m_MyCube->m_size*m_size / 2, 0, -m_MyCube->m_size*m_size / 2);

    glEnd();

    glPopMatrix();
}


void Enviroment::timerTick(){


    /*for (auto bullet : m_projectiles){
        int i=-1;
        i++;
        if (bullet!=nullptr){
            if (bullet->isActive){
                bullet->moveForward();
            }else{
               m_projectiles.at(i)=nullptr;
               //m_projectiles.erase(std::find(m_projectiles.begin(),m_projectiles.end(),bullet));
                //delete bullet;
            }
        }
	}*/
	lichyCyklus=!lichyCyklus;
	if (lichyCyklus){
	for (int i=0;i<m_projectiles.size();i++){
        if (m_projectiles.at(i)!=nullptr){
            if (m_projectiles.at(i)->isActive){
                m_projectiles.at(i)->moveForward();
            }else{
                 delete m_projectiles.at(i);
                 m_projectiles.at(i)=nullptr;
            }
        }
	}

	for (auto explo : m_explosions){
        if (explo!=nullptr){
            explo->update();
        }
	}
	for (auto cube : m_animList){
        if (cube!=nullptr){
            cube->animate();
        }
	}
	for (auto team : m_cubesByTeam){
        for(auto cube : team){
            if (cube!=nullptr){
                cube->update();
            }
        }
	}
	//m_MyCube->m_energy++;

	}
    glutPostRedisplay();

}
Enviroment::~Enviroment(){
//log.close();
}
void Enviroment::inicVectorsAndVariables(){
    lichyCyklus=true;
    m_nextID=1;
    m_sizeOfGrid = 3;
    std::vector<GraphObj*> row;

	for (int i=0;i<m_size;i++){
        row.push_back(nullptr);
	}
	for (int i=0;i<m_size;i++){
        m_env.push_back(row);
	}
	m_cubesByTeam.push_back(std::vector<Cube*>() );
    m_cubesByTeam.push_back(std::vector<Cube*>() );
}

Enviroment::Enviroment(std::ifstream& levelMap){
    int sizeOfMap=0;

    std::string input;
    levelMap >> sizeOfMap;
    if (sizeOfMap==0){
        Enviroment(51);
    }else{
        m_size=sizeOfMap;
        inicVectorsAndVariables();
        for (int j=0;j<sizeOfMap;j++){
            std::vector<GraphObj*> row;
            for (int i=sizeOfMap-1;i>=0;i--){
                levelMap >> input;
                if (input=="w"){
                    m_env.at(i).at(j) = new Wall(3,6,{0.1,0.1,0.1},3,this);
                }else if(!input.empty() && input.find_first_not_of("0123456789") == std::string::npos){
                    new EnemyCube(this,{i,j},std::atoi(input.c_str()));
                }else if (input=="me"){
                    m_MyCube=new MyCube(this,{i,j});
                    m_env.at(i).at(j)=m_MyCube;
                }else{

                }
            }
            m_env.push_back(row);
        }
    }
     TestSuite::Suite tests("Test Analyticke geometrie");
     tests.addTest(new TestAnGeo);
     tests.run();
     tests.report();
     //logThis(tests.reportS());


}

Enviroment::Enviroment(int size) {
    m_size=size;
    m_nextID=1;
    //getObjAtPos_succesCount=0;
    //log.open("C:\\Users\\stras_000\\Documents\\log.txt",std::ios::trunc);

    inicVectorsAndVariables();
	m_MyCube=new MyCube(this,{3,3});

	//m_MyCube->setPosition({2,1});
	/*new Cube(3,{1,0,0},this,{1,3},1);

	new Cube(3,{1,0,0},this,{3,5},1);
	new Cube(3,{1,1,0},this,{7,8},1);
	new Cube(3,{1,1,0},this,{7,20},1);
	new Cube(3,{1,1,0},this,{20,38},1);
	new Cube(3,{0.4,1,0.5},this,{30,34},1);
	new Cube(3,{1,1,0},this,{31,33},1);
	new Cube(3,{0.4,1,0.5},this,{31,32},1);
	new Cube(3,{1,1,0},this,{35,28},1);
	new Cube(3,{0.4,1,0.5},this,{40,24},1);*/

	new EnemyCube(this,{12,7},1);
    new EnemyCube(this,{20,38},1);
    new EnemyCube(this,{31,33},2);
    new EnemyCube(this,{35,44},2);
    new EnemyCube(this,{31,42},3);
    new EnemyCube(this,{2,12},1);
    new EnemyCube(this,{3,15},2);
    new EnemyCube(this,{2,27},3);
    new EnemyCube(this,{7,18},2);
    new EnemyCube(this,{9,29},2);

    for (int i=0;i<size;i++){
        //m_env.at(i).at(9)=new Wall(3,6,{0.1,0.1,0.1},m_MyCube->m_size);
        m_env.at(i).at(size-1)=new Wall(3,6,{0.1,0.1,0.1},m_MyCube->m_size,this);
        m_env.at(0).at(i)=new Wall(3,6,{0.1,0.1,0.1},m_MyCube->m_size,this);
        m_env.at(size-1).at(i)=new Wall(3,6,{0.1,0.1,0.1},m_MyCube->m_size,this);
        m_env.at(i).at(0)=new Wall(3,6,{0.1,0.1,0.1},m_MyCube->m_size,this);
    }


}

bool Enviroment::moveMyCube(int wasd) {
    CordXY pozice=m_MyCube->getPosition();

    float fi=m_MyCube->getFiAsAngle();          //ot�cen� podle toho kam se d�v�me, aby dopredu bylo dopredu
    float pi=3.14159265358979f;
    float hranice[]={pi/4,(pi/4)*3,(pi/4)*5,(pi/4)*7};
    std::vector<CordXY> smery={{0,1},{-1,0},{0,-1},{1,0}};
    if (fi>hranice[0] && fi<hranice[1]){
        wasd=(wasd+1)%4;
    }else if (fi>hranice[1] && fi<hranice[2]){
        wasd=(wasd+2)%4;
    }else if (fi>hranice[2] && fi<hranice[3]){
        wasd=(wasd+3)%4;
    }
    m_MyCube->move(smery.at(wasd));
    /*
    try {
        if (m_env.at(pozice.x+smery.at(wasd).x).at(pozice.y+smery.at(wasd).y)==nullptr){
            m_env.at(pozice.x+smery.at(wasd).x).at(pozice.y+smery.at(wasd).y)=m_MyCube;
            m_env.at(pozice.x).at(pozice.y)=nullptr;
            m_MyCube->setPosition({pozice.x+smery.at(wasd).x,pozice.y+smery.at(wasd).y});
            return true;
        }else {
            return false;
        }
    }catch (std::out_of_range){
        return false;
    }*/

}
bool Enviroment::moveCube(Cube* cube,CordXY direction){
    CordXY posInEnv=cube->getPosition();
    try {
        if (m_env.at(posInEnv.x+direction.x).at(posInEnv.y+direction.y)==nullptr){
            m_env.at(posInEnv.x+direction.x).at(posInEnv.y+direction.y)=cube;
            m_env.at(posInEnv.x).at(posInEnv.y)=nullptr;
            cube->setPosition({posInEnv.x+direction.x,posInEnv.y+direction.y});
            for (auto team : m_cubesByTeam){
                for (auto cube : team){
                    if (cube!=nullptr){
                    cube->checkPossibleTargets();
                    }
                }
            }
            return true;
        }else {
            return false;
        }
    }catch (std::out_of_range){
        return false;
    }

}
void Enviroment::removeObj(GraphObj* obj){
/*for (int i=0;i<m_size;i++){
        for (int j=0;j<m_size;j++){
            if (dynamic_cast<Cube*>(m_env.at(i).at(j))->getID==obj->){
                m_env.at(i).at(j)==nullptr;
            }
        }
	}*/
	throw;
}
void Enviroment::removeCube(Cube* obj){
    if (obj!=nullptr){

        for (int i=0;i<m_size;i++){
            for (int j=0;j<m_size;j++){

                if (m_env.at(i).at(j)!=nullptr){
                    if (m_env.at(i).at(j)->getID()==obj->getID()){
                    m_env.at(i).at(j)=nullptr;
                }
            }
        }
	}
	for (int i=0;i<m_cubesByTeam.size();i++){
        for (int j=0;j<m_cubesByTeam.at(i).size();j++){
            if(m_cubesByTeam.at(i).at(j)!=nullptr && m_cubesByTeam.at(i).at(j)->getID()==obj->getID()){
                m_cubesByTeam.at(i).at(j)=nullptr;
            }
        }
	}
    }


}
void Enviroment::removeExplosion(Explosion* explo){
    /*if (obj!=nullptr){
        for (int i=0;i<m_explosions.size(),i++){
            o
        }
    }*/

    auto positionIt = std::find(m_explosions.begin(),m_explosions.end(),explo);
    if (positionIt!=m_explosions.end()){
        m_explosions.erase(positionIt);
        //logThis("explo erased");
        delete explo;                                   //zpusobuje paad
    }

}



GraphObj* Enviroment::getObjAtPos(CordXY pos){
    if (pos.x>=0 && pos.y>=0){
        try {
            return m_env.at(pos.x).at(pos.y);
        }catch (std::out_of_range){
            return nullptr;
        }
    }
    return nullptr;
}
CordXY Enviroment::whereAmI(CordXYZ pos){
    try {            // sem tam to tu spadne :(
        CordXY result={0,0};
        for (result.x=0;result.x<m_size;result.x++){
            for (result.y=0;result.y<m_size;result.y++){
                if ((pos.x > result.x*m_sizeOfGrid) && (pos.x < (result.x+1)*m_sizeOfGrid) && ((-1)*pos.z > result.y*m_sizeOfGrid) && ((-1)*pos.z <(result.y+1)*m_sizeOfGrid)){

                    return result;
                }
            }
        }
    }catch (int e){
        return {-1,-1};
    }
    return {-1,-1};

}
CordXY Enviroment::whereAmI(GraphObj* whoAmI){
    CordXY result={0,0};
    for (result.x=0;result.x<m_size;result.x++){
        for (result.y=0;result.y<m_size;result.y++){
            if (m_env.at(result.x).at(result.y)!=nullptr && (m_env.at(result.x).at(result.y)->getID()==whoAmI->getID())){
                return result;
            }
        }
    }
    return {-1,-1};
}
void Enviroment::addCubeToTeam(Cube* cube, int team){
    try{
        m_cubesByTeam.at(cube->getTeam()).push_back(cube);
    }catch(std::out_of_range){
        //logThis(cube->getPosition());
        //logThis(cube->getTeam());
        //logThis(" ");
    }
}

void Enviroment::addObjToPos(CordXY posInEnv,GraphObj* obj){
    m_env.at(posInEnv.x).at(posInEnv.y)=obj;

}

void Enviroment::addActiveProjectile(Projectile* bullet){
    m_projectiles.push_back(bullet);
}
int Enviroment::GrabID(){
//logThis((CordXYZ){m_nextID,0,0});
return m_nextID++;
}
void Enviroment::logThis(std::string what){
log<<what<<std::endl;
}
void Enviroment::logThis(CordXYZ what){
log<<"("<<what.x<<", "<<what.y<<", "<<what.z<<")"<< std::endl;
}
void Enviroment::logThis(float what){
log<<what<< std::endl;
}
void Enviroment::logThis(double what){
log<<what<< std::endl;
}
void Enviroment::logThis(CordXY what){
log<<what.x<<" "<<what.y<< std::endl;
}
void Enviroment::addToAnimeList(Cube* c){
    bool alreadyThere=false;
    for (auto cube : m_animList){
        if(cube!=nullptr){
            if (cube->getID()==c->getID()){
                alreadyThere=true;
            }
        }
    }
    if (!alreadyThere){
        m_animList.push_back(c);
    }
}
void Enviroment::removeFromAnimeList(Cube* c){
    auto positionIt = std::find(m_animList.begin(),m_animList.end(),c);
    if (positionIt!=m_animList.end()){
        m_animList.erase(positionIt);
    }
/*
    for (int i=0;i<m_animList.size();i++){
        if (m_animList.at(i)!=nullptr && m_animList.at(i)->getID()==c->getID()){
            m_animList.at(i)=nullptr;
        }
    }*/
}
#endif
