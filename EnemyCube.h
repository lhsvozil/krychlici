#ifndef _ENCUBEH
#define _ENCUBEH

#include "Cube.h"
#include "CordXYZ.h"
#include "CordXY.h"
#include "Energy.h"

class EnemyCube : public Cube /// krychlik co ma jakousi zakladni Umelou inteligenci, po zarazeni do stejneho tymu muze byt i friendly
{
protected:
    int m_howFarISee;   /// z jak velkeho okoli si vybira cile
    Cube* m_enemyInSight;
public:
    EnemyCube(Enviroment* env,CordXY posInEnv,int lvl);

    Cube* getNearestEnemy();
    void checkPossibleTargets();
    void update();
};
#endif
