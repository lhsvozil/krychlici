#include "anGeo.h"
#include <math.h>
#include <assert.h>
#include <stdlib.h>


float AnGeo::distanceOfPoints(CordXYZ a,CordXYZ b){
        return sqrtf((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z));
    }
float AnGeo::distanceOfPoints(CordXY a,CordXY b){
        return sqrtf((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
    }
float AnGeo::getViewAngle(float r, float sideA){
        assert(r/sideA>-1&&r/sideA<1);
        return asin(r/sideA);

    }
CordXYZ AnGeo::randomRangePoint(float minimum, float maximum){
    CordXYZ result;
    result.x=((maximum-minimum)*((float)rand()/RAND_MAX))+minimum;
    result.y=((maximum-minimum)*((float)rand()/RAND_MAX))+minimum;
    result.z=((maximum-minimum)*((float)rand()/RAND_MAX))+minimum;
return result;
}
float AnGeo::randomRangef(float minimum, float maximum){
    return ((maximum-minimum)*((float)rand()/RAND_MAX))+minimum;
}

CordXYZ AnGeo::normalizeVec(CordXYZ vec){
    float sizeOfVec=sqrtf(vec.x*vec.x+vec.y*vec.y+vec.z*vec.z);
    CordXYZ result={vec.x/sizeOfVec,vec.y/sizeOfVec,vec.z/sizeOfVec};
    return result;
}
CordXY AnGeo::normalizeVec(CordXY vec){
    float sizeOfVec=sqrtf(vec.x*vec.x+vec.y*vec.y);
    CordXY result={vec.x/sizeOfVec,vec.y/sizeOfVec};
    return result;
}
float AnGeo::sizeOfVec(CordXYZ vec){
    return sqrtf(vec.x*vec.x+vec.y*vec.y+vec.z*vec.z);
}
float AnGeo::vectorToAngleDeg(CordXY vec){
    if (vec.x==0 && vec.y==0){
        return 0;
    }
    return atan2(vec.y,vec.x)*180/3.14159265358979f;
}
