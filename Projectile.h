#ifndef _PROJECTILEH
#define _PROJECTILEH

#include "Color.h"
#include "GraphObj.h"
#include "CordXYZ.h"
#include "Enviroment.h"

class Projectile {

private:
	int m_dmg;
	float m_size;
	Color m_color;
	float m_speed;
    CordXYZ m_directionVector;
    CordXYZ m_position;
    Enviroment* MyEnv;
    int m_whoShotMe;
public:
    bool isActive;
	Projectile(const Projectile& vzor);                 /// Kopírovací kontstruktor, kazda zbran ma svuj projektil, ktery se pri vystreleni pouze skopíruje
                                                        /// a prida se do m_projectiles vectoru v Enviroment, ktery uz se postara o vykreslovani a volani Projectile::moveForward()
    Projectile(Enviroment* env,float speed,Color color,float dmg,float size, int whoOwnsMe);
    //~Projectile();
	void moveForward();
    void setDirection(CordXYZ dirVec);
    void setPosition(CordXYZ pos);
	void draw();
	int getDmg();
};

#endif
