#ifndef _COLORH
#define _COLORH

struct Color {

	float red;
	float green;
	float blue;

	Color operator+ (const Color& other){
        Color result={red+other.red,green+other.green,blue+other.blue};
        return result;
	}
	Color operator- (const Color& other){
        Color result={red-other.red,green-other.green,blue-other.blue};
        return result;
	}

};
#endif
