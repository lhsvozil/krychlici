#include "EnemyCube.h"
#include "AnGeo.h"
#include "Enviroment.h"
#include "Weapon.h"
#include "AnGeo.h"
Cube* EnemyCube::getNearestEnemy(){
    float nearestDist=99999;
    Cube* nearestCube=nullptr;

    for (auto team : MyEnv->m_cubesByTeam){
        for(auto cube : team){
          if (cube != nullptr){
            if (cube->getTeam()==m_team){
                break;
            }else{
                if (AnGeo::distanceOfPoints(getRealPosition(),cube->getRealPosition())<nearestDist){
                    nearestDist=AnGeo::distanceOfPoints(getRealPosition(),cube->getRealPosition());
                    nearestCube=cube;
                }
            }
          }
        }
    }
    return nearestCube;
}
void EnemyCube::update(){
    m_energy++;
    if (m_energy.shooting>m_weapon->m_energyConsumption && m_enemyInSight!=nullptr){
        changeMyDirection(m_enemyInSight->getPosition()-getPosition());
        Fire();
        m_energy.shooting-=m_weapon->m_energyConsumption;
    }
}

EnemyCube::EnemyCube(Enviroment* env,CordXY posInEnv,int lvl):Cube(3,{0,0,0},env,posInEnv,1){
    //lvl inic here
    if (lvl == 1){
        m_color={1,0,0};
        m_hp=60;
        m_howFarISee=15;
    }else if(lvl == 2){
        m_color={1,1,0};
        m_hp=80;
        m_howFarISee=20;
    }else if(lvl == 3){
        m_color={1,1,1};
        m_hp=100;
        m_howFarISee=35;
        m_energy.replenishRate=2;
    }
    m_enemyInSight=nullptr;
    m_originalColor=m_color;



}
void EnemyCube::checkPossibleTargets(){
    Cube* possibleTarget=getNearestEnemy();
    if (AnGeo::distanceOfPoints(m_position,possibleTarget->getPosition())<m_howFarISee){

        m_enemyInSight=possibleTarget;
    }else{
        m_enemyInSight=nullptr;
    }
}

