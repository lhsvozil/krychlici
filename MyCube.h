#ifndef _MYCUBEH
#define _MYCUBEH


#include "CordXYZ.h"
#include "Cube.h"

class Enviroment;

class MyCube : public Cube /// muj krychlik obsahujici hlavne kamerove zalezitosti
{

private:
    float m_fi;        //promenna hodnota k parametrickemu vyjadreni kruznice
    CordXY m_cameraDistance;
    float m_cameraLookAtDistance;
    float m_cameraLookAtY;
    bool dead;
public:

	MyCube(Enviroment* env, CordXY positionInEnv);
    CordXYZ getCameraPos();
    CordXYZ getCameraLookAtPos();
    void setFi(float fi);
    void setCameraLookAtY(float y);
    float getFiAsAngle();
    void changeCameraDist(int increaseOrDecrease);

    void Fire();
    void die();

};
#endif
