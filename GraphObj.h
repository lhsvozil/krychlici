#ifndef _GRAPHOBJH
#define _GRAPHOBJH

#include "CordXY.h"
#include "CordXYZ.h"
//#include "Projectile.h"

class Projectile;
class Cube;
class GraphObj {


public:
    int m_ID;
    float m_size;
	virtual void draw(CordXY position) = 0;
    virtual void takeHit(Projectile* from);
    virtual void takeHit(Cube* from);
    int getID();
    virtual CordXYZ getRealPosition(); //wtf
protected:
	CordXY m_position;
	CordXYZ m_realPosition;


};
#endif
