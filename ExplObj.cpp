#include "ExplObj.h"
#include <math.h>
#include <random>


ExplObj::ExplObj(CordXYZ position, CordXYZ direction)
{
   m_position=position;
   m_directionVec=direction;
   m_transparency=1;
}
void ExplObj::fade(){
   m_transparency-=1/80.0f;
}
