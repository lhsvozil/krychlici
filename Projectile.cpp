#include "Projectile.h"
#include <GL/glu.h>
#include <GL/glut.h>
#include "Color.h"
#include <cmath>
#include "AnGeo.h"

Projectile::Projectile(const Projectile& vzor) {
	MyEnv=vzor.MyEnv;
	m_color=vzor.m_color;
	m_size=vzor.m_size;
	m_speed=vzor.m_speed;
	m_dmg=vzor.m_dmg;
    m_position=vzor.m_position;
    m_whoShotMe=vzor.m_whoShotMe;
    m_directionVector={1,0,0};
	isActive=true;
}
Projectile::Projectile(Enviroment* env,float speed,Color color,float dmg,float size, int whoOwnsMe){
    MyEnv=env;
    m_speed=speed;
    m_color=color;
    m_dmg=dmg;
    m_size=size;
    m_whoShotMe=whoOwnsMe;
}
/*Projectile::~Projectile(){
    MyEnv=nullptr;
}*/
void Projectile::setDirection(CordXYZ dirVec){
    m_directionVector=dirVec;
}
void Projectile::setPosition(CordXYZ pos){
    m_position=pos;
}
int Projectile::getDmg(){
    return m_dmg;

}
void Projectile::moveForward() {
    //MyEnv->logThis(m_position);
	m_position.x+=m_directionVector.x*m_speed;
	m_position.y+=m_directionVector.y*m_speed;
	m_position.z+=m_directionVector.z*m_speed;

	CordXY posInEnv = MyEnv->whereAmI(m_position);
	if (MyEnv->getObjAtPos(posInEnv)!=nullptr){
        if (MyEnv->getObjAtPos(posInEnv)->m_ID!=m_whoShotMe){
            isActive=false;
            MyEnv->getObjAtPos(posInEnv)->takeHit(this);
            return;
        }

        //
	}else{

        CordXY smery[]={{0,1},{1,1},{1,0},{1,-1},{0,-1},{-1,-1},{-1,0},{-1,1}};
        for (int i=0;i<8;i++){
            GraphObj* obj = MyEnv->getObjAtPos(posInEnv+smery[i]);
            if (obj!=nullptr && obj->getID()!=m_whoShotMe){
                /*MyEnv->logThis(posInEnv+smery[i]);
                MyEnv->logThis(m_position);
                MyEnv->logThis(obj->getRealPosition());
                MyEnv->logThis(AnGeo::distanceOfPoints(m_position,obj->getRealPosition()));*/
                if (AnGeo::distanceOfPoints(m_position,obj->getRealPosition()) < 0.95*m_size+obj->m_size*0.5){
                    isActive=false;
                    obj->takeHit(this);
                    break;
                }
            }
        }
	}

}



void Projectile::draw(){
    glPushMatrix();
    glTranslatef(m_position.x,m_position.y,m_position.z);

    glColor3f(m_color.red,m_color.green,m_color.blue);
    glutSolidSphere(m_size,12,12);

    glPopMatrix();
}
