#ifndef TETRAH_H
#define TETRAH_H

#include "ExplObj.h"
#include "Color.h"
class TetraH : public ExplObj /// malinkaty deformovany (podle CordXYZ size) tetraHedronek (4 sten) - v explozi
{
    public:
        TetraH(CordXYZ position, CordXYZ direction, CordXYZ rotationAngleVec,float angleChange,CordXYZ size, Color color);
        virtual ~TetraH();
        void draw();
        void fly();
    protected:
        float m_fadeFactor;
        CordXYZ m_rotationAngleVec;
        float m_angle;
        float m_angleChange;
    private:
};

#endif // TETRAH_H
