#ifndef _WEAPCPP
#define _WEAPCPP
#include "Projectile.h"
#include "CordXY.h"
class Weapon {

public:
	Projectile m_projectile;

    Weapon(Enviroment* env,float speed,Color color,float dmg,float size, int whoOwnsMe);
	void draw(CordXY position);
	int m_energyConsumption;
};

#endif
