#include "TetraH.h"
#include <GL/glu.h>
#include <GL/glut.h>

TetraH::TetraH(CordXYZ position, CordXYZ direction, CordXYZ rotationAngleVec,float angleChange,CordXYZ size, Color color):ExplObj(position,direction)
{
    m_color=color;
    m_size=size;
    m_fadeFactor=1;
    m_rotationAngleVec=rotationAngleVec;
    m_angleChange=angleChange;
    m_angle=5*m_angleChange;
}

TetraH::~TetraH()
{
    //dtor
}

void TetraH::draw(){
    glPushMatrix();
    glTranslatef(m_position.x,m_position.y,m_position.z);

    glColor4f(m_color.red,m_color.green,m_color.blue,m_transparency);
    glScalef(m_size.x,m_size.y,m_size.z);
    glRotatef(m_angle,m_rotationAngleVec.x,m_rotationAngleVec.y,m_rotationAngleVec.z);
    glutSolidTetrahedron();

    glPopMatrix();
}
void TetraH::fly(){
    m_position=m_position+m_directionVec;
    //m_directionVec.x=m_directionVec.x*m_fadeFactor;
    //m_directionVec.z=m_directionVec.z*m_fadeFactor;
    if (m_position.y>-1.5){                                         // posunuta zem
        m_directionVec.y=m_directionVec.y-0.07;
    }else {
        if (m_directionVec.y < (-0.1)){
            m_directionVec.y=-0.5*m_directionVec.y;
            m_position=m_position+m_directionVec;

            //m_fadeFactor=0.99;
        }else{
            m_position.y=-1.5;
            m_directionVec.y=0;
            m_directionVec.x=0;
            m_directionVec.z=0;
            m_angleChange=0;
        }
    }




    m_angle=m_angle+m_angleChange;
    m_angleChange*=m_fadeFactor;
    m_fadeFactor*=m_fadeFactor;

}
