
#include <iostream>
#include <stdlib.h>
#include <fstream>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glu.h>
#include <GL/glut.h>
#endif
#include "Enviroment.h"
#include "CordXY.h"


Enviroment* MyEnv;
bool firstGo = true;
CordXY lastMousePos;
CordXY windowSize;

using namespace std;

const float BOX_SIZE = 7.0f;

void handleKeypress(unsigned char key, int x, int y) {
	switch (key) {
    case 'w':
        MyEnv->moveMyCube(0);
        break;
    case 'a':
        MyEnv->moveMyCube(1);
        break;
    case 's':
        MyEnv->moveMyCube(2);
        break;
    case 'd':
        MyEnv->moveMyCube(3);
        break;
    case 'f':
        glutFullScreen();
        break;
	case 27: //Escape key
	    //MyEnv.~Enviroment();
		exit(0);
	case 43:
        MyEnv->m_MyCube->changeCameraDist(-1);
        break;
    case 45:
        MyEnv->m_MyCube->changeCameraDist(1);
        break;
	}

	glutPostRedisplay();
}




void initRendering() {
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_NORMALIZE);
	glEnable(GL_COLOR_MATERIAL);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable( GL_BLEND );
    glClearColor(0.0,0.0,0.0,0.0);


}
void handleResize(int w, int h) {
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (float)w / (float)h, 1.0, 200.0);

	glutWarpPointer(w/2,h/2);
    windowSize={w,h};


}
void mouseMove(int x, int y){
    if (firstGo){
        lastMousePos={x,y};
        firstGo=false;
    }else
    {
       /* if (lastMousePos.x-x<0){
            MyEnv.m_MyCube->m_directionVector.x+=0.5;
        }else{
            MyEnv.m_MyCube->m_directionVector.x-=0.5;
        }*/
        MyEnv->m_MyCube->setFi((lastMousePos.x-x)/250.0);
        MyEnv->m_MyCube->setCameraLookAtY((lastMousePos.y-y)/35.0);
        lastMousePos={x,y};
    }
    if (x<windowSize.x/5 or x>4*windowSize.x/5){
        firstGo=true;
        glutWarpPointer(windowSize.x/2,y);
    }
    if (y<windowSize.y/5 or y>4*windowSize.y/5){
        firstGo=true;
        glutWarpPointer(x,windowSize.y/2);
    }
   //glutPostRedisplay();
}

	GLfloat lightColor[] = {0.7f, 0.7f, 0.7f, 1.0f};
	GLfloat lightColor2[] = {0.1f, 0.1f, 0.1f, 1.0f};
	GLfloat lightPos[] = {-50, 7, 50, 1.0f};
	GLfloat lightPos2[] = {50, 20, -200, 1.0f};
	GLfloat ambientLight[] = {0.3f, 0.3f, 0.3f, 1.0f};

void drawScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	//glTranslatef(-8.0f, -5.0f, -20.0f);
	CordXYZ camera = MyEnv->m_MyCube->getCameraPos();
	CordXYZ camLookAt = MyEnv->m_MyCube->getCameraLookAtPos();

    gluLookAt(camera.x,camera.y,camera.z,camLookAt.x,camLookAt.y,camLookAt.z,0,1,0);

    //gluLookAt((MyCubePos.x+0.5)*MyEnv.m_MyCube->m_size,3,-MyCubePos.y*MyEnv.m_MyCube->m_size+5,(MyCubePos.x+0.5)*MyEnv.m_MyCube->m_size+MyEnv.m_MyCube->m_directionVector.x,0+MyEnv.m_MyCube->m_directionVector.y,-(MyCubePos.y+3)*MyEnv.m_MyCube->m_size+MyEnv.m_MyCube->m_directionVector.z,0,1,0);
	//gluLookAt(MyCubePos.x*MyEnv.m_MyCube->m_size,3,-MyCubePos.y*MyEnv.m_MyCube->m_size+5,MyCubePos.x*MyEnv.m_MyCube->m_size,0,-MyCubePos.y*MyEnv.m_MyCube->m_size,0,1,0);
	//glTranslatef(-MyCubePos.x*MyEnv.m_MyCube->m_size-2.0f,-2.5f,MyCubePos.y*MyEnv.m_MyCube->m_size-8.0f);
    //glRotatef(20, 1.0f, 0.0f, 0.0f);




	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientLight);


	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT1, GL_POSITION, lightPos2);

	//glRotatef(-_angle, 1.0f, 1.0f, 0.0f);

	MyEnv->drawScene();


	glutSwapBuffers();
}

void mouseClick(int button, int state, int x, int y){
    if (button==GLUT_LEFT_BUTTON && state==GLUT_DOWN){
        MyEnv->m_MyCube->Fire();
    }else if(button==GLUT_RIGHT_BUTTON && state==GLUT_DOWN){
        MyEnv->m_MyCube->attack();
    }

}
void timerTick(int value){
    MyEnv->timerTick();
    glutTimerFunc(15, timerTick, 0); // puv 25

}


int main(int argc, char** argv) {
    std::ifstream mapa;
    mapa.open("1.txt");
    if (mapa.is_open()){
        MyEnv = new Enviroment(mapa);
    }else{
        MyEnv= new Enviroment(51);
    }


	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH | GLUT_ALPHA);
	glutInitWindowSize(400, 400);


	glutCreateWindow("pff");
	glutSetCursor(GLUT_CURSOR_NONE);
	initRendering();

	glutDisplayFunc(drawScene);
	glutKeyboardFunc(handleKeypress);
	glutReshapeFunc(handleResize);

	glutPassiveMotionFunc(mouseMove);
	glutMouseFunc(mouseClick);
	glutTimerFunc(225, timerTick, 0);

	glutMainLoop();
	return 0;
}
