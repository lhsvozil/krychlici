#ifndef _CORDXYH
#define _CORDXYH
struct CordXY   /// 2d bod
{
	float x;
	float y;

	CordXY operator+ (const CordXY& other){
        CordXY result={x+other.x,y+other.y};
        return result;
	}
	CordXY operator- (const CordXY& other){
        CordXY result={x-other.x,y-other.y};
        return result;
	}
	CordXY operator* (const float number){
        CordXY result={x*number,y*number};
        return result;
	}

};


#endif
