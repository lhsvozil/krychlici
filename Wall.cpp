#ifndef _WALLCPP
#define _WALLCPP

#include "Wall.h"
#include <GL/glu.h>
#include <GL/glut.h>

void Wall::draw(CordXY position) {
	glPushMatrix();
    glTranslatef((position.x+0.5)*m_myCubeSize,0,-(position.y+0.5)*m_myCubeSize);

	glBegin(GL_QUADS);

	//Top face
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glNormal3f(0.0, 1.0f, 0.0f);
	glVertex3f(-m_size / 2, m_height / 2, -m_size / 2);
	glVertex3f(-m_size / 2, m_height / 2, m_size / 2);
	glVertex3f(m_size / 2, m_height / 2, m_size / 2);
	glVertex3f(m_size / 2, m_height / 2, -m_size / 2);

	//Bottom face
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glNormal3f(0.0, -1.0f, 0.0f);
	glVertex3f(-m_size / 2, -m_myCubeSize / 2, -m_size / 2);
	glVertex3f(m_size / 2, -m_myCubeSize / 2, -m_size / 2);
	glVertex3f(m_size / 2, -m_myCubeSize / 2, m_size / 2);
	glVertex3f(-m_size / 2, -m_myCubeSize / 2, m_size / 2);

	//Left face
	glNormal3f(-1.0, 0.0f, 0.0f);
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(-m_size / 2, -m_myCubeSize / 2, -m_size / 2);
	glVertex3f(-m_size / 2, -m_myCubeSize / 2, m_size / 2);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-m_size / 2, m_height / 2, m_size / 2);
	glVertex3f(-m_size / 2,m_height / 2, -m_size / 2);

	//Right face
	glNormal3f(1.0, 0.0f, 0.0f);
	glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(m_size / 2, -m_myCubeSize / 2, -m_size / 2);
	glVertex3f(m_size / 2, m_height / 2, -m_size / 2);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(m_size / 2, m_height / 2, m_size / 2);
	glVertex3f(m_size / 2, -m_myCubeSize / 2, m_size / 2);


	//Front face
	glNormal3f(0.0, 0.0f, 1.0f);
glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(-m_size / 2, -m_myCubeSize / 2, m_size / 2);

	glVertex3f(m_size / 2, -m_myCubeSize / 2, m_size / 2);

	glVertex3f(m_size / 2, m_height / 2, m_size / 2);

	glVertex3f(-m_size / 2, m_height / 2, m_size / 2);

	//Back face
	glNormal3f(0.0, 0.0f, -1.0f);
glColor3f(m_color.red,m_color.green, m_color.blue);
	glVertex3f(-m_size / 2, -m_myCubeSize / 2, -m_size / 2);

	glVertex3f(-m_size / 2, m_height / 2, -m_size / 2);

	glVertex3f(m_size / 2, m_height / 2, -m_size / 2);

	glVertex3f(m_size / 2, -m_myCubeSize / 2, -m_size / 2);

	glEnd();
	glPopMatrix();
}

Wall::Wall(float size, float height, Color color, float myCubeSize, Enviroment* env) {
	m_size=size;
	m_height=height;
	m_color=color;
	m_myCubeSize=myCubeSize;
	MyEnv=env;
	m_ID=MyEnv->GrabID();
	m_position=MyEnv->whereAmI(this);
    m_realPosition.x= m_position.x*m_size+0.5*m_size;
    m_realPosition.z= m_position.y*m_size+0.5*m_size;
    m_realPosition.z*=-1;
    m_realPosition.y=0;


}
#endif




