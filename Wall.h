#ifndef _WALLH
#define _WALLH

#include "GraphObj.h"
#include "Color.h"
#include "Projectile.h"
#include "Cube.h"

class Wall : public GraphObj {

private:

	float m_height;
	Color m_color;
    float m_myCubeSize;
    Enviroment* MyEnv;
public:
	void draw(CordXY position);

	Wall(float size, float height, Color color, float myCubeSize, Enviroment* env);


};
#endif
